const { ObjectID, ObjectId } = require("bson");
const { getConnection } = require("../../../configs/db"); 
const CustomError = require("../../../errors/custom-error");
const { assign } = require("../methods/gen-reflect");

class GenRepository {
    entityClass;
    constructor(entityClass){
        this.entityClass = entityClass;
    } 
    getCollection(){
        return getConnection().collection(this.entityClass.collection)
    }

    static getCollectionFrom(collection){
        return getConnection().collection(collection);
    }
    /**
     * 
     * @param {any[]} entities 
     * @returns any
     */
    async insert(entities, schemaName='schema'){
        //const toInsert  = entities.map(elmt=> assign(this.entityClass, elmt, schemaName));
        try{
            const toInsert = entities;
            const collection = getConnection().collection(this.entityClass.collection);
            return await collection.insertMany(toInsert);    
        }catch(e){
            let message = "Erreur lors de l'insertion";
            console.log(e);
            if(e.message) message = e.message;
            throw new CustomError(message);
        }
    }
   
    async insertWithSession(entities, session){
        //const toInsert  = entities.map(elmt=> assign(this.entityClass, elmt, schemaName));
        try{
            const toInsert = entities;
            const collection = getConnection().collection(this.entityClass.collection);
            return await collection.insertMany(toInsert, { session });    
        }catch(e){
            let message = "Erreur lors de l'insertion";
            console.log(e);
            if(e.message) message = e.message;
            throw new CustomError(message);
        }
    }

    /**
     * 
     * @param {{
     * 
     *  excludeFields?: string[],
     *  pagination?: {
     *              page: string, 
    *               pageElmtCount: string,
    *               orderBy: {column: string, order: 'asc'|'desc'}[]
     *          },
     *  filter?:[
     *      {
     *          column: string, 
     *          value: string,
     *          type: string,
     *          comparator: string
     *      }
     *  ],
     * filterMode: 'and' | 'or'
     * }} options 
     * @returns any[]
     * 
     * pagination[page] is a string because of queryParams
     */
    async find(params){
        let projection ={};
        if(params.excludeFields){
            params.excludeFields.forEach(element => {
                projection[element]=0;
           });
        }
        let queryOptions = {}; 
        let createPaginationOptions = GenRepository.createPaginationOptions(params.pagination);
        queryOptions = {...queryOptions, ...createPaginationOptions};
      
      
        const filters = GenRepository.createMatchOptions(params.filter, params.filterMode)
        console.log('filters', JSON.stringify(filters))
        const collection = this.getCollection();
        const results = await collection.find(filters,queryOptions).project(projection).toArray();
         
        const ans =  {
            data: results
                .map(elmt => Object.assign(new this.entityClass, elmt)),
            meta: {
                totalElmtCount: (await collection.countDocuments(filters))
            }
        };
        console.log('ans', ans)
        return ans;
    }

    async findById(_id, excludeFields=null){
        const filter = [{
            column: '_id',
            type: 'string',
            value: ObjectID(_id),
            comparator: '='
        }];
        const result = await this.find({filter, excludeFields});
        if(result.data.length === 0) return null;
        return result.data[0];
    }


    static createPaginationOptions(pagination, sortObject=false){
        if(!pagination) return {}; 
        const ans = {
            ...this.myCreatePaginationOptions(pagination),
            ...this.createSortOPtions(pagination.orderBy, sortObject)
        } ;
       
        return ans;
    }

    static myCreatePaginationOptions(pagination){ 
        if(!pagination) return {};
        const ans = {
            limit: +pagination.pageElmtCount,
            skip: pagination.pageElmtCount * (pagination.page-1),
        } ;
       
        return ans;
    }

    static createSortOPtions(sort, object=false){
        if(!sort) sort = [];
        if(sort.length == 0) sort.push({column: '_id', order: 'asc'});
        
        if(object) {
            const ans = {};
            sort.forEach((elmt) => {
                ans[elmt.column] = elmt.order == 'asc' ? 1 : -1;
            })
            return {sort: ans};
        }
        return {
            sort: sort.map(elmt => [elmt.column, elmt.order])
        } ;
    }
    
    async update(entity) {
        const collection = getConnection().collection(this.entityClass.collection);
        const id = entity._id;
        delete entity._id;
        return await collection.updateOne({_id: ObjectID(id)}, {$set: entity});
    }

    async delete(id){
        const collection = getConnection().collection(this.entityClass.collection);
        return await collection.deleteOne({_id: ObjectID(id)});
    }

    static createMatchOptions(filters, filterMode){
        if(!filters || filters.length === 0) return {}
        const comparators = {
            "=": "$eq",
            "<": "$lt",
            ">": "$gt",
            "<=": "$lte",
            ">=": "$gte",
            "exists" : "$exists",
            "!=": '$ne'
        };
        
       const ans = filters.map(filter => {
            const value=this.parseValue(filter.value, filter.type);
            if(filter.comparator === "like"){
                return {[filter.column]:{'$regex': value, '$options': 'i' }};
            }if(filter.comparator === "notExistsOrNull"){
                return {'$or': [ { [filter.column]:{$exists: false}}, { [filter.column]:{$eq: null}}] };
            }
            const f = {[filter.column]: { [comparators[filter.comparator]]: value}};
            return f;
        })
        return { [filterMode ==='or'?'$or': '$and']: ans };
    }
    static parseValue(value, type){
        if(type === "int" || type === "float" || type === "number")
            return +value;
        if(type === "date")
            return new Date(value);
        if(type === "ObjectId")
            return new ObjectId(value);
        return value;
    }
    async softDelete(id){
        const collection = this.getCollection();
        const deletedAt = new Date();
        return await collection.updateOne({_id: ObjectID(id)}, {$set: {deletedAt}});
    }
}

module.exports = GenRepository;