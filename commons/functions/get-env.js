const CustomError = require("../../errors/custom-error") 
const dotenv = require('dotenv');
const path = require('path');
const envLocalPath = path.resolve(__dirname, '../..', '.env.local');

if (dotenv.config({ path: envLocalPath }).error) {
    require('dotenv').config()
}
require('dotenv').config({ path: '.env.local' }); 


module.exports = function(key){
    if( process.env[key] === null ||  process.env[key] === undefined)
        throw new CustomError("unknown env key: "+key)
    return process.env[key];
}