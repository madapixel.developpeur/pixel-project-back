const generator = require('generate-password');

module.exports = function (length = 10){
    return generator.generate({
        length,
        numbers: true
    });
}