const { body } = require("express-validator");

class MessageSeen {
    static schema = {
        "userFromId": {
            type: "string"
        },
        "userToId": {
            type: "string"
        },
        "lastSeenDate": {
            type: "date"
        },
        "lastMessageId": {
            type: "string"
        }
    }
    static collection = "MessageSeen";
}
module.exports = MessageSeen;