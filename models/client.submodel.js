const { body } = require("express-validator");
const Constant = require("./constant.model");

class Client {
    static schema = {
        "nom": {
            type: 'string', 
            validatorGetter: (paramPropertyName='nom')=> 
                body(paramPropertyName).isString().trim().notEmpty().withMessage("Nom invalide")
        },
        "prenom": {
            type: 'string', 
            validatorGetter: (paramPropertyName='prenom')=> 
                body(paramPropertyName).isString().trim().notEmpty().withMessage("prénom invalide")
        },
        "pays": {
            type: 'string', 
            validatorGetter: (paramPropertyName='pays')=> 
                body(paramPropertyName).isString().withMessage("Pays invalide")
        },
        "adresse": {
            type: 'string', 
            validatorGetter: (paramPropertyName='adresse')=> 
                body(paramPropertyName).isString().withMessage("Adresse invalide")
        },
        "province": {
            type: 'string', 
            validatorGetter: (paramPropertyName='province')=> 
                body(paramPropertyName).optional({checkFalsy: true}).isString()
        },
        "codePostal": {
            type: 'string', 
            validatorGetter: (paramPropertyName='codePostal')=> 
                body(paramPropertyName).optional({checkFalsy: true}).isString()
        },
        "cheminLogo": {
            type: 'string', 
            validatorGetter: (paramPropertyName='cheminLogo')=> 
                body(paramPropertyName).optional({checkFalsy: true}).isString()
        },
        "significationLogo": {
            type: 'string', 
            validatorGetter: (paramPropertyName='significationLogo')=> 
                body(paramPropertyName).optional({checkFalsy: true}).isString()
        },
        "type": {
            type: "int",
            validatorGetter: (paramPropertyName='type')=> 
                body(paramPropertyName).isInt().toInt().isIn([Constant.typeClient.particulier, Constant.typeClient.societe])
        }
    };
    static createSchemaDto = {...this.schema};
    static updateSchemaDto = {...this.schema};
}
module.exports = Client;