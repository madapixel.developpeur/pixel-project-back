const { body } = require("express-validator");
const ProjectFile = require("./project-file.submodel");

class ProjectDetails {
    static schema = {
        "name": {
            type: 'string', 
            validatorGetter: (paramPropertyName='name')=> 
                body(paramPropertyName).isString().trim().notEmpty().withMessage("Nom du client ou de l'entreprise invalide")
        },
        "description": { 
            type: "string", 
            validatorGetter: (paramPropertyName='description') => 
                body(paramPropertyName).optional({checkFalsy: true}).isString().withMessage("Description du client ou de l'entreprise invalide")
        },
        "fichiers": {classConstructor: ProjectFile, isArray: true },
    };
    static createSchemaDto = {...this.schema};
    static updateSchemaDto = {...this.schema};
}
module.exports = ProjectDetails;