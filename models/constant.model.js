const { body } = require("express-validator");

class Constant {
    static status = {
        deleted : -1,
        created : 0,
        validated : 1
    };
    
    static projectStatus = {
        deleted : -1,
        creating : 0,
        created : 1,
        updated: 2,
        validatedByAdmin : 3,
        todosValidatedByClient: 4,
        finished: 5,
        billed: 6
    };

    static todoStatus = {
        deleted : -1,
        created: 0,
        inProgress: 1,
        finished: 2,
        validated: 3,
    }

    static typeClient = {
        particulier: 1,
        societe: 2
    };

    static roles = {
        client: 1,
        admin: 2
    }

    static messageStatus = {
        deleted: 0,
        sent: 1,
        seen: 2,
    }
    static tva = 20;
}
module.exports = Constant;