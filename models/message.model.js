const { body } = require("express-validator");
const ProjectFile = require("./project-file.submodel");
const _id = {
    type: 'string',
    validatorGetter: (paramPropertyName='_id')=> body(paramPropertyName).isString().withMessage('Identifiant invalide') 
}
class Message {
    static collection = "Message";
    static schema = {
        // "userFromId": {
        //     type: 'string', 
        //     validatorGetter: (paramPropertyName='userFromId')=> 
        //         body(paramPropertyName).isString().withMessage("Identifiant de l'envoyeur invalide")
        // },
        "userToId": {
            type: 'string', 
            validatorGetter: (paramPropertyName='userToId')=> 
                body(paramPropertyName).isString().withMessage("Identifiant du destinataire invalide")
        },
        // "sentDate": { 
        //     type: 'date' 
        // },
        // "seenDate": { 
        //     type: 'date' 
        // },
        "contenu": { 
            type: "string", 
            validatorGetter: (paramPropertyName='contenu') => 
                body(paramPropertyName).optional({checkFalsy: true}).isString().withMessage("Contenu du message invalide")
        },
        "fichiers": {classConstructor: ProjectFile, isArray: true },
        // "status": { 
        //     type: 'int' 
        // },
    };
    static createSchemaDto = {...this.schema};
    static updateSchemaDto = {...this.schema, _id};
}
module.exports = Message;