const { body } = require("express-validator");
const ProjectFile = require("./project-file.submodel");

class Subproject {
    static schema = {
        "nom": {
            type: 'string', 
            validatorGetter: (paramPropertyName='name')=> 
                body(paramPropertyName).isString().trim().notEmpty().withMessage("Nom du projet invalide")
        },
        "description": { 
            type: "string", 
            validatorGetter: (paramPropertyName='description') => 
                body(paramPropertyName).isString().withMessage("Description ou object du projet invalide")
        },
        "nbrPersonnesDecisionnaires": {
            type: "int",
            validatorGetter: (paramPropertyName='nbrPersonnesDecisionnaires') => 
                body(paramPropertyName).isInt().withMessage("nombre de personnes décisionnaires du projet invalide").toInt()
        },
        "souciAncienLogo": { 
            type: "string", 
            validatorGetter: (paramPropertyName='description') => 
                body(paramPropertyName).optional({checkFalsy: true}).isString()
        },
        "identiteNouveauLogo": { 
            type: "string", 
            validatorGetter: (paramPropertyName='description') => 
                body(paramPropertyName).optional({checkFalsy: true}).isString()
        },
        "fichiers": {classConstructor: ProjectFile, isArray: true },
    };
    static createSchemaDto = {...this.schema};
    static updateSchemaDto = {...this.schema};
}
module.exports = Subproject;