const { body } = require("express-validator");

class Answers {
    static collection = "Answers";
    static schema = {
        "parent": { 
            type: "string", 
            validatorGetter: (paramPropertyName='parent') => 
                body(paramPropertyName).optional({checkFalsy: true}).isString().withMessage("Parent invalide")
        },
        "categories": { 
            type: "object", 
            validatorGetter: (paramPropertyName='categories') => 
                body(paramPropertyName).optional({checkFalsy: true}).isObject({strict:true}).withMessage("Categories invalides")
        },
    };
    static createSchemaDto = {...this.schema};
    static updateSchemaDto = {...this.schema};
}
module.exports = Answers;