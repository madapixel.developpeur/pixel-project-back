const { body } = require("express-validator");
const md5 = require("md5");

class User {
    static collection = "User";
    static schema = {
        "firstName": {
            type: 'string', 
            validatorGetter: (paramPropertyName='firstName')=> 
                body(paramPropertyName).isString().withMessage("Prenom invalide")
        },
        "lastName": {
            type: 'string', 
            validatorGetter: (paramPropertyName='lastName')=> 
                body(paramPropertyName).isString().withMessage("Nom invalide")
        }/* ,
        "password":  {
            type: 'string', 
            validatorGetter: (paramPropertyName='password')=> 
                body(paramPropertyName).isString().withMessage("Mot de passe invalide").customSanitizer(elmt => md5(elmt))
        } */,
        "email": {
            type: 'string', 
            validatorGetter: (paramPropertyName='email')=> 
                body(paramPropertyName).isEmail().withMessage("Email invalide").trim().toLowerCase()
        },
        "image": {
            type: 'string', 
            validatorGetter: (paramPropertyName='image')=> 
                body(paramPropertyName).optional({checkFalsy: true}).isString().trim().withMessage("Chemin du fichier invalide")
        },
        
        // "roleId": {
        //     type: 'int'
        // }
    }
    static createSchemaDto = (()=> {
        const ans = { ...User.schema,
            /* "confirmPassword":  {
                type: 'string', 
                validatorGetter: (paramPropertyName='confirmPassword')=> 
                    body(paramPropertyName).isString().withMessage("Confirmation de mot de passe invalide").customSanitizer(elmt => md5(elmt))
            } */
        };
        delete ans.role;
        return ans;
    } )()
    static updateSchemaDto = {...this.schema}
    static apiSchemaDto = (()=> {
        const ans = { 
            "firstName": {
                type: 'string', 
                validatorGetter: (paramPropertyName='firstName')=> 
                    body(paramPropertyName).isString().withMessage("Prénom invalide")
            },
            "lastName": {
                type: 'string', 
                validatorGetter: (paramPropertyName='lastName')=> 
                    body(paramPropertyName).isString().withMessage("Nom invalide")
            },
            "email": {
                type: 'string', 
                validatorGetter: (paramPropertyName='email')=> 
                    body(paramPropertyName).isEmail().withMessage("Email invalide").trim().toLowerCase()
            },
            "ibi": {
                type: 'string', 
                validatorGetter: (paramPropertyName='ibi')=> 
                    body(paramPropertyName).isString().withMessage("IBI invalide")
            },
        };
        return ans;
    } )()


    static loginSchemaDto = {
        "email": {
            type: 'string', 
            validatorGetter: (paramPropertyName='email')=> 
                body(paramPropertyName).isEmail().withMessage("Email invalide")
        },
        "password":  {
            type: 'string', 
            validatorGetter: (paramPropertyName='password')=> 
                body(paramPropertyName).isString().withMessage("Mot de passe invalide")
        },
    }
    static canAccessDto = {
        "roleIds":  {
            type: 'array', 
            validatorGetter: (paramPropertyName='roleId')=> 
                body(paramPropertyName).isArray().withMessage("roleId invalide")
        },
    }
    static updatePasswordSchemaDto = {
        "currentPassword":  {
            type: 'string', 
            validatorGetter: (paramPropertyName='currentPassword')=> 
                body(paramPropertyName).isString().withMessage("Mot de passe actuel invalide")
        },
        "newPassword":  {
            type: 'string', 
            validatorGetter: (paramPropertyName='newPassword')=> 
                body(paramPropertyName).isString().withMessage("Nouveau mot de passe invalide").customSanitizer(elmt => md5(elmt))
        },
        "confirmNewPassword":  {
            type: 'string', 
            validatorGetter: (paramPropertyName='confirmNewPassword')=> 
                body(paramPropertyName).isString().withMessage("Confirmation du nouveau mot de passe invalide").customSanitizer(elmt => md5(elmt))
        },
    }
}
module.exports = User;