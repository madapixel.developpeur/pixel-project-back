const {body} = require('express-validator'); 
const Constant = require('./constant.model');
const ProjectFile = require('./project-file.submodel');

const _id = {
    type: 'string',
    validatorGetter: (paramPropertyName='_id')=> body(paramPropertyName).isString().withMessage('Identifiant invalide') 
}
const projectId = {
    type: 'string',
    validatorGetter: (paramPropertyName='projectId')=> body(paramPropertyName).isString().withMessage("Identifiant du projet invalide")
}
class Todo {
    static schema ={
        "name" : { type: 'string',  validatorGetter: (paramPropertyName='name')=> body(paramPropertyName).isString().withMessage("Label d'elements de reparations invalides").trim()},
        "description": { type: 'string',  validatorGetter: (paramPropertyName='description')=> body(paramPropertyName).optional({checkFalsy: true}).isString().withMessage("Description d'elements de reparations invalides")},
        // "creationDate": { 
        //     type: 'date' 
        // },
        // "inProgressDate": { 
        //     type: 'date' 
        // },
        // "finishedDate": { 
        //     type: 'date' 
        // },
        // "validatedDate": {
        //     type: 'date' 
        // },
        // "status": { 
        //     type: 'int'
        // },
        // "progression": { 
        //     type: 'float' 
        // },
    }
    static createSchemaDto = {...this.schema, 
        projectId,
        apercu: {classConstructor: ProjectFile, isArray: true }
    };
    static updateSchemaDto = {
        ...this.schema, 
        _id, 
        apercu: {classConstructor: ProjectFile, isArray: true },
        dateLivraison: {
            type: 'date',  
            validatorGetter: (paramPropertyName='dateLivraison')=> body(paramPropertyName).optional({checkFalsy: true}).isISO8601().withMessage("Date de livraison invalide").toDate()
        },
        progression: {
            type: 'float',  
            validatorGetter: (paramPropertyName='progression')=> body(paramPropertyName).optional().isFloat({min: 0}).withMessage("Progression invalide").toFloat()
        },
    };
    static updateProgressionSchemaDto = {
        _id, 
        progressionDate: {
            type: 'date',  
            validatorGetter: (paramPropertyName='progressionDate')=> body(paramPropertyName).isISO8601().withMessage("Date de progression invalide").toDate()
        },
        status: {
            type: 'int',
            validatorGetter: (paramPropertyName='progressionDate')=> body(paramPropertyName).isIn([Constant.todoStatus.created, Constant.todoStatus.inProgress, Constant.todoStatus.finished, Constant.todoStatus.validated]).withMessage("Statut invalide").toInt()
        },
    };
    static updateOnlyProgressionValueSchemaDto = {
        _id, 
        progression: {
            type: 'float',  
            validatorGetter: (paramPropertyName='progression')=> body(paramPropertyName).isFloat({min: 0}).withMessage("Progression invalide").toFloat()
        },
    }
    static collection = "Todo";
}

module.exports = Todo;