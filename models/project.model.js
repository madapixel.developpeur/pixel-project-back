const {body} = require('express-validator'); 
const Answers = require('./answers.model');
const Client = require('./client.submodel');
const CompanyOrClient = require('./client.submodel');
const ProjectDetails = require('./project-details.submodel');
const Subproject = require('./subproject.submodel');
const TypeProject = require('./type-project.model');
const _id = {
    type: 'string',
    validatorGetter: (paramPropertyName='_id')=> body(paramPropertyName).isString().withMessage('Identifiant invalide') 
}
class Project {
    static collection = "Project";

    static schema = {
        "type": {classConstructor: TypeProject},
        "answers": {classConstructor: Answers},
        "details": {classConstructor: ProjectDetails, isArray: true },
        "userId": {
            type: 'string',
            validatorGetter: (paramPropertyName='userId') => 
                body(paramPropertyName).isString().withMessage("Client invalide")
        },
        
    };

    static createSchemaDto = (() => {
        const ans = {...Project.schema};
        delete ans.details;
        return ans;
    })();
    static updateSchemaDto = (() => {
        const ans = {...Project.schema, _id};
        delete ans.userId;
        return ans;
    })();
    static apiSchemaDto = {
        "type": {classConstructor: TypeProject},
        "userId": {
            type: 'string',
            validatorGetter: (paramPropertyName='userId') => 
                body(paramPropertyName).isString().withMessage("Client invalide")
        },
        
    };
    
    
    static billingSchemaDto = {
        _id,
        signaturePath: { 
            type: 'string',
            validatorGetter: (paramPropertyName='signaturePath')=> body(paramPropertyName).isString().withMessage('Chemin du signature invalide')  
        }
    }

    static updateDateLivraisonSchemaDto = {
        dateLivraison: {
            type: 'date',  
            validatorGetter: (paramPropertyName='dateLivraison')=> body(paramPropertyName).isISO8601().withMessage("Date de livraison invalide").toDate()
        },
        
    };
    
}

module.exports = Project;