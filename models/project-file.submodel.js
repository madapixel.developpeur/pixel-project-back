const { body } = require("express-validator");

class ProjectFile {
    static schema = {
        "chemin": {
            type: 'string', 
            validatorGetter: (paramPropertyName='name')=> 
                body(paramPropertyName).isString().trim().notEmpty().withMessage("Chemin du ficher invalide")
        },
        "nom": { 
            type: "string", 
            validatorGetter: (paramPropertyName='nom') => 
                body(paramPropertyName).isString().withMessage("Nom du fichier invalide")
        },
    };
    static createSchemaDto = {...this.schema};
    static updateSchemaDto = {...this.schema};
}
module.exports = ProjectFile;