const { body } = require("express-validator");

class Questions {
    static schema = {
        "categories": {
            type: 'string', 
            isArray: true,
            validatorGetter: (paramPropertyName='categories')=> 
                body(paramPropertyName).isArray().withMessage("Catégories invalides")
        },
        "parent": { 
            type: "string", 
            validatorGetter: (paramPropertyName='parent') => 
                body(paramPropertyName).isString().withMessage("Parent invalide")
        },
    };
    static createSchemaDto = {...this.schema};
    static updateSchemaDto = {...this.schema};
}

class TypeProject {
    static collection = "TypeProject";
    static schema = {
        "nom": { 
            type: "string", 
            validatorGetter: (paramPropertyName='nom') => 
                body(paramPropertyName).isString().withMessage("Type du projet invalide")
        },
        "_id": {
            type: 'string',
            validatorGetter: (paramPropertyName='_id')=> body(paramPropertyName).isString().withMessage('Identifiant invalide') 
        },
        "role": {
            type: 'string',
            validatorGetter: (paramPropertyName='role')=> body(paramPropertyName).isString().withMessage('Role invalide') 
        },
        "questions": {classConstructor: Questions}

    };
    static createSchemaDto = {...this.schema};
    static updateSchemaDto = {...this.schema};
    static apiSchemaDto = {...this.schema};
}
module.exports = TypeProject;