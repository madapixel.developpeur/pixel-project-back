const { body } = require("express-validator");
const ProjectFile = require("./project-file.submodel");
const _id = {
    type: 'string',
    validatorGetter: (paramPropertyName='_id')=> body(paramPropertyName).isString().withMessage('Identifiant invalide') 
}
class TodoComment {
    static collection = "TodoComment";
    static schema = {
        
        
        "todoId": {
            type: 'string', 
            validatorGetter: (paramPropertyName='todoId')=> 
                body(paramPropertyName).isString().withMessage("Identifiant de la tâche invalide")
        },
        "contenu": { 
            type: "string", 
            validatorGetter: (paramPropertyName='contenu') => 
                body(paramPropertyName).optional({checkFalsy: true}).isString().withMessage("Contenu de la commentaire invalide")
        },
        "fichiers": {classConstructor: ProjectFile, isArray: true },
        
    };
    static createSchemaDto = {...this.schema};
    static updateSchemaDto = {...this.schema, _id};
}
module.exports = TodoComment;