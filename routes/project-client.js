var express = require('express'); 
const createRouteCallback = require('../commons/functions/create-route-callback');
const GenRepository = require('../commons/database/class/gen-repository');
const TypeProject = require('../models/type-project.model');
const { ObjectId } = require('mongodb');
const createAuth = require('../middlewares/auth');
const Constant = require('../models/constant.model');
const TokenRepository = require('../repositories/token.repo');
const User = require('../models/user.model');
const { assign } = require('../commons/database/methods/gen-reflect');
const UserService = require('../services/user.service');
const CustomError = require('../errors/custom-error');
const genPassword = require('../commons/functions/gen-password');
const md5 = require('md5');
const MailerService = require('../services/mailer.service');
const ProjectService = require('../services/project.service');
const Project = require('../models/project.model');
const { getClient, transactionOptions } = require('../configs/db');
var router = express.Router();

const userRepository = new GenRepository(User);
const tokenRepository = new TokenRepository();
var typeProjectRepository = new GenRepository(TypeProject);

const signin = async function (user, options){
    const newUser = assign(User, user, "apiSchemaDto");
    const oldUser = await UserService.findUserByEmail(newUser.email);
    if(oldUser !== null) {
        throw new CustomError("Adresse email déjà utilisée");
    }
    const plainPassword = genPassword();
    newUser.password = md5(plainPassword);
    newUser.roleId = Constant.roles.client;

    const userCollection = GenRepository.getCollectionFrom(User.collection);
    await userCollection.insertOne(newUser, options);
    
    return {
        newUser: newUser,
        plainPassword: plainPassword
    };
}

const createClientAndProjet = async function (req, res){
    const session = getClient().startSession();
    const options = {session};
    try {
        session.startTransaction(transactionOptions);
        const {newUser, plainPassword} = await signin(req.body?.client, options);

        const typeProject = await typeProjectRepository.findById(req.body?.typeProjectId);
        if(typeProject == null) {
            throw new CustomError("Ce type de projet n'existe pas");
        }
        var project = {
            type: typeProject,
            userId: newUser._id
        };
       
        const projectCollection = GenRepository.getCollectionFrom(Project.collection);
        project.creationDate = new Date();
        project.details = [];
        project.userId = new ObjectId(project.userId);
        project.status = Constant.projectStatus.created;
        await projectCollection.insertOne(project, options);
        await session.commitTransaction();  

        // Send mail
        MailerService.sendEmailNewClient(newUser, plainPassword);
        res.json({userId: newUser._id});
    } catch(err){
        await session.abortTransaction();
        throw err;
    } finally {
        await session.endSession();
    }
}

router.post('/', createRouteCallback(createClientAndProjet));

module.exports = router;
