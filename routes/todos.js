var express = require('express'); 
const createRouteCallback = require('../commons/functions/create-route-callback');
const createBodySchemaParser = require('../middlewares/body-schema-parser');
const {assign} = require('../commons/database/methods/gen-reflect');
const CustomError = require('../errors/custom-error');
const createAuth = require('../middlewares/auth');
const Constant = require('../models/constant.model');
const Todo = require('../models/todo.model');
const TodoService = require('../services/todo.service');
var router = express.Router();


const createTodoAdmin = async function(req, res) {
  const body = assign(Todo, req.body, "createSchemaDto");
  const result = await TodoService.createTodo(body);
  res.json(result);
}

const updateTodoAdmin = async function(req, res) {
  const body = assign(Todo, req.body, "updateSchemaDto");
  const result = await TodoService.updateTodo(body);
  res.json(result);
}

const deleteTodoAdmin = async function(req, res) {
   await TodoService.deleteTodo(req.params.id);
   res.json({message: 'A faire supprimé'});
}

const getTodoByIdAdmin = async function(req, res) {
  const result = await TodoService.findCoreById(req.params.id, {exists: true});
  res.json(result);
}

const getTodosAdmin = async function(req, res) {
  const result = await TodoService.findTodosByProject(req.query.projectId, req.query);
  res.json(result);
}

const updateProgressionAdmin = async function(req, res){
    const body = assign(Todo, req.body, "updateProgressionSchemaDto");
    const options = {currentUser: (req.currentUser.roleId == Constant.roles.admin ? null : req.currentUser)};
    const result = await TodoService.updateProgression(body, options);
    res.json(result);
}

const updateOnlyProgressionValueAdmin = async function(req, res){
    const body = assign(Todo, req.body, "updateOnlyProgressionValueSchemaDto");
    const result = await TodoService.updateProgressionValue(body);
    res.json(result);
}

const getTodosClient = async function(req, res) {
    const result = await TodoService.findTodosByProject(req.query.projectId, req.query, {currentUser: req.currentUser});
    res.json(result);
  }



//Client endpoints
router.get('/client', createAuth([Constant.roles.client]), createRouteCallback(getTodosClient));

//Admin endpoints
router.get('/admin', createAuth([Constant.roles.admin]), createRouteCallback(getTodosAdmin));
router.get('/admin/:id', createAuth([Constant.roles.admin]), createRouteCallback(getTodoByIdAdmin));
router.post('/admin', createAuth([Constant.roles.admin]), createBodySchemaParser(Todo, "createSchemaDto"), createRouteCallback(createTodoAdmin));
router.patch('/admin',createAuth([Constant.roles.admin]),createBodySchemaParser(Todo, 'updateSchemaDto'), createRouteCallback(updateTodoAdmin));
router.delete('/admin/:id',createAuth([Constant.roles.admin]), createRouteCallback(deleteTodoAdmin));
router.patch('/admin/progression-value',createAuth([Constant.roles.admin]), createBodySchemaParser(Todo, "updateOnlyProgressionValueSchemaDto"), createRouteCallback(updateOnlyProgressionValueAdmin));


//Common endpoints
router.get('/common/:id',createAuth([]), createRouteCallback(getTodoByIdAdmin));
router.patch('/common/progression',createAuth([]), createBodySchemaParser(Todo, "updateProgressionSchemaDto"), createRouteCallback(updateProgressionAdmin));



module.exports = router;
