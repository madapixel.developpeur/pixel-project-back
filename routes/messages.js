var express = require('express'); 
const createRouteCallback = require('../commons/functions/create-route-callback');
const createBodySchemaParser = require('../middlewares/body-schema-parser');
const {assign} = require('../commons/database/methods/gen-reflect');
const CustomError = require('../errors/custom-error');
const createAuth = require('../middlewares/auth');
const Constant = require('../models/constant.model');
const Message = require('../models/message.model');
const MessageService = require('../services/message.service');
const { ObjectId } = require('mongodb');
var router = express.Router();


const sendMessage = async function(req, res) {
  const body = assign(Message, req.body, "createSchemaDto");
  const result = await MessageService.send(body, req.currentUser, req.app.io);
  res.json(result);
}

const getChatUsers = async function(req, res) {  
  const result = await MessageService.findChatUsers(req.currentUser, req.query);
  res.json(result);
}

const getMessages = async function(req, res) {
  const result = await MessageService.findMessages(req.currentUser, new ObjectId(req.params.userId), req.query);
  res.json(result);
}

const getMessageById = async function(req, res) {
  const result = await MessageService.findMessageById(new ObjectId(req.params.id), req.currentUser);
  res.json(result);
}

const getSeenUser = async function(req, res) {
  const result = await MessageService.findSeenUser(new ObjectId(req.params.userId), req.currentUser);
  res.json(result);
}

const setSeenDateNow = async function(req, res) {
  const result = await MessageService.setSeenDateNow(req.currentUser, new ObjectId(req.params.userId), req.app.io);
  res.json(result);
}

const getSeenById = async function(req, res) {
  const result = await MessageService.findSeenById(new ObjectId(req.params.id), req.currentUser);
  res.json(result);
}

const getMessagesNotSeen = async function(req, res) {
  const result = await MessageService.findMessagesNotSeen(req.currentUser);
  res.json(result);
}

//Common endpoints
router.post('/send', createAuth([]), createBodySchemaParser(Message, "createSchemaDto"), createRouteCallback(sendMessage));
router.get('/users', createAuth([]), createRouteCallback(getChatUsers));
router.get('/not-seen', createAuth([]), createRouteCallback(getMessagesNotSeen));
router.get('/:userId/all', createAuth([]), createRouteCallback(getMessages));
router.get('/:id', createAuth([]), createRouteCallback(getMessageById));
router.post('/seen/:userId', createAuth([]), createRouteCallback(setSeenDateNow));
router.get('/seen/:id', createAuth([]), createRouteCallback(getSeenById));
router.get('/seen/:userId/user', createAuth([]), createRouteCallback(getSeenUser));

module.exports = router;
