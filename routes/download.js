var express = require('express'); 
const path = require('path');
var router = express.Router();

router.get('', (req, res, next) => {
    const filePath = req.query.filePath;
    res.sendFile(filePath, {root: './'}, (err) => {
        if (err) {
            next(err);
        } else {
            console.log('File Sent:', filePath);
        }
    });
});

module.exports = router;