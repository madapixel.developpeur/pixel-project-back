var express = require('express'); 
const upload = require('../middlewares/upload');
const getFileNameWithoutExt = require('../commons/functions/get-filename-without-ext');
const uploadSingle = upload.single('file');
const uploadMultiple = upload.array('files');
var router = express.Router();

router.post('/', function (req, res) {
    uploadSingle(req, res, function (err) {
        if (err) {
            res.status(500).json("Une erreur s'est produite...");
            return;
        } 
        if(!req.file){
            res.status(500).json("Fichier introuvable");
            return;
        }
            
        res.json(req.file.path);
    });
});

router.post('/multiple', function (req, res) {
    uploadMultiple(req, res, function (err) {
        if (err) {
            res.status(500).json("Une erreur s'est produite...");
            return;
        } 
        const result = req.files.map((file) => {
            return {
                originalname: file.originalname, 
                path: file.path,
                nameWithoutExt: getFileNameWithoutExt(file.originalname)
            };
        });
        res.json(result);
    });
});

module.exports = router;
