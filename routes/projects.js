var express = require('express'); 
const createRouteCallback = require('../commons/functions/create-route-callback');
const createBodySchemaParser = require('../middlewares/body-schema-parser');
const {assign} = require('../commons/database/methods/gen-reflect');
const CustomError = require('../errors/custom-error');
const createAuth = require('../middlewares/auth');
const Project = require('../models/project.model');
const Constant = require('../models/constant.model');
const ProjectService = require('../services/project.service');
const TodoService = require('../services/todo.service');
const TypeProject = require('../models/type-project.model');
const Subproject = require('../models/subproject.submodel');
const Client = require('../models/client.submodel');
const { ObjectId } = require('mongodb');

var router = express.Router();

const createProject = async function(req, res) {
  const body = assign(Project, req.body, "createSchemaDto");
  const result = await ProjectService.insertUpdate(body, true, req.currentUser);
  res.json(result);
}

const updateProject = async function(req, res) {
  const body = assign(Project, req.body, "updateSchemaDto");
  const result = await ProjectService.insertUpdate(body, false, req.currentUser);
  res.json(result);
}

const getProjectById = async function(req, res) {
  const result = await ProjectService.findCoreProjectById(req.params.id, {exists: true, currentUser: req.currentUser});
  res.json(result);
}

const getProjectsAdmin = async function(req, res) {
  //const result = await ProjectService.findProjects(req.query);
  const result = await ProjectService.findProjectsWithRoles(req.query, req.currentUser);
  console.log(result);
  res.json(result);
}

const getProjectByIdAdmin = async function(req, res) {
  const result = await ProjectService.findCoreProjectById(req.params.id, {exists: true});
  res.json(result);
}

const changeStatus = async function(status, req, res) {
  const options = {};
  if(req.currentUser.roleId === Constant.roles.client) options.currentUser = req.currentUser;
  await ProjectService.changeStatus(req.params.id, status, options);
  res.json({message: 'statut modifié'});
}

const getProjectProgressionAdmin = async function(req, res) {
  const result = await TodoService.findProjectProgression(req.params.id, {exists: true});
  res.json(result);
}

const getProjectsClient = async function(req, res) {
  const result = await ProjectService.findProjects(req.query, {currentUser: req.currentUser});
  res.json(result);
}
const getProjectProgressionClient = async function(req, res) {
  const result = await TodoService.findProjectProgression(req.params.id, {exists: true, currentUser: req.currentUser});
  res.json(result);
}
const billProjectClient = async function(req, res) {
  const body = assign(Project, req.body, "billingSchemaDto");
  await ProjectService.bill(body, {exists: true, currentUser: req.currentUser});
  res.json({message: 'Projet facturé'});
}

const updateDateLivraison = async function(req, res) {
  const body = assign(Project, req.body, "updateDateLivraisonSchemaDto");
  body._id = new ObjectId(req.params.id);
  await ProjectService.updateDateLivraison(body);
  res.json('Date de livraison modifiée');
}




//Client endpoints
router.post('/create', createAuth([Constant.roles.client, Constant.roles.admin]), createBodySchemaParser(Project, "createSchemaDto"), createRouteCallback(createProject));
router.post('/update', createAuth([Constant.roles.client, Constant.roles.admin]), createBodySchemaParser(Project, "updateSchemaDto"), createRouteCallback(updateProject));
router.get('/client/:id', createAuth([Constant.roles.client]), createRouteCallback(getProjectById));
router.patch('/client/:id/validate-todos', createAuth([Constant.roles.client]), createRouteCallback(changeStatus.bind(undefined, Constant.projectStatus.todosValidatedByClient)));
router.get('/client', createAuth([Constant.roles.client]), createRouteCallback(getProjectsClient));
router.get('/client/:id/progression', createAuth([Constant.roles.client]), createRouteCallback(getProjectProgressionClient));
router.patch('/client/:id/finish', createAuth([Constant.roles.client]), createRouteCallback(changeStatus.bind(undefined, Constant.projectStatus.finished)));
router.patch('/client/bill', createAuth([Constant.roles.client]), createBodySchemaParser(Project, "billingSchemaDto"), createRouteCallback(billProjectClient));

//Admin endpoints
router.get('/admin', createAuth([Constant.roles.admin]), createRouteCallback(getProjectsAdmin));
router.get('/admin/:id', createAuth([Constant.roles.admin]), createRouteCallback(getProjectByIdAdmin));
router.patch('/admin/:id/validate', createAuth([Constant.roles.admin]), createRouteCallback(changeStatus.bind(undefined, Constant.projectStatus.validatedByAdmin)));
router.get('/admin/:id/progression', createAuth([Constant.roles.admin]), createRouteCallback(getProjectProgressionAdmin));
router.patch('/admin/:id/livraison', createAuth([Constant.roles.admin]), createBodySchemaParser(Project, "updateDateLivraisonSchemaDto"), createRouteCallback(updateDateLivraison));

module.exports = router;