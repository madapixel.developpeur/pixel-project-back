var express = require('express'); 
const createRouteCallback = require('../commons/functions/create-route-callback');
const createBodySchemaParser = require('../middlewares/body-schema-parser');
const {assign} = require('../commons/database/methods/gen-reflect');
const CustomError = require('../errors/custom-error');
const createAuth = require('../middlewares/auth');
const Constant = require('../models/constant.model');
const TodoComment = require('../models/todo-comment.model');
const TodoCommentService = require('../services/todo-comment.service');
var router = express.Router();


const createComment = async function(req, res) {
  const body = assign(TodoComment, req.body, "createSchemaDto");
  const result = await TodoCommentService.createComment(body, req.currentUser);
  res.json(result);
}

const getComments = async function(req, res) {
  const result = await TodoCommentService.findComments(req.params.todoId);
  res.json(result);
}


//Common endpoints
router.get('/common/:todoId',createAuth([]), createRouteCallback(getComments));
router.post('/common/',createAuth([]), createBodySchemaParser(TodoComment, "createSchemaDto"),  createRouteCallback(createComment));

module.exports = router;
