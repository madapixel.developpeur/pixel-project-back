var express = require('express'); 
const createRouteCallback = require('../commons/functions/create-route-callback');
const GenRepository = require('../commons/database/class/gen-repository');
const TypeProject = require('../models/type-project.model');
const { ObjectId } = require('mongodb');
const createAuth = require('../middlewares/auth');
const Constant = require('../models/constant.model');
var router = express.Router();

var typeProjectRepository = new GenRepository(TypeProject);
const getTypes = async function(req, res) {
    const result = await typeProjectRepository.find({});
    res.json(result.data);
}
const getTypesWithRole = async function (req, res){
    let user = req.currentUser;
    let params = {};
    if(user && user.role != null) {
        params.filter = [];
            params.filter.push({
                column: 'role',
                type: 'string',
                value: user.role,
                comparator: '='
            });
    }
    const result = await typeProjectRepository.find(params);
    res.json(result.data);
}

router.get('/', createAuth([Constant.roles.admin, Constant.roles.client]), createRouteCallback(getTypesWithRole));

module.exports = router;
