var express = require('express'); 
const GenRepository = require('../commons/database/class/gen-repository');
const { assign } = require('../commons/database/methods/gen-reflect');
const createRouteCallback = require('../commons/functions/create-route-callback');
const CustomError = require('../errors/custom-error');
const createBodySchemaParser = require('../middlewares/body-schema-parser');
const User = require('../models/user.model'); 
const TokenRepository = require('../repositories/token.repo');
const UserService = require('../services/user.service');
const MailerService = require('../services/mailer.service');
const createAuth = require('../middlewares/auth');
const Constant = require('../models/constant.model');
const genPassword = require('../commons/functions/gen-password');
const { ObjectId } = require('mongodb');
const md5 = require('md5');
var router = express.Router();

const userRepository = new GenRepository(User);
const tokenRepository = new TokenRepository();

const signin = async function (req, res){
    const newUser = assign(User, req.body);
    const oldUser = await UserService.findUserByEmail(newUser.email);
    if(oldUser !== null) {
        throw new CustomError("Adresse email déjà utilisée");
    }
    const plainPassword = genPassword();
    newUser.password = md5(plainPassword);
    newUser.roleId = Constant.roles.client;
    await userRepository.insert([newUser]);
    // Send mail
    MailerService.sendEmailNewClient(newUser, plainPassword);
    res.json({userId: newUser._id});
}

const updateInfos = async function (req, res){
    const newUser = assign(User, req.body, "updateSchemaDto");
    const oldUser = await UserService.findUserByEmail(newUser.email);
    if(oldUser !== null && !oldUser._id.equals(req.currentUser._id)) {
        throw new CustomError("Adresse email déjà utilisée");
    }
    newUser._id = req.currentUser._id;
    await userRepository.update(newUser);
    const result = await userRepository.findById(req.currentUser._id, ['password']);
    res.json(result);
}

const updatePassword = async function (req, res){
    const data = assign(User, req.body, "updatePasswordSchemaDto");
    const checkUser = await UserService.findUserByEmailAndPassword({email: req.currentUser.email, password: data.currentPassword});
    if(checkUser == null) throw new CustomError("Mot de passe actuel invalide");
    if(data.newPassword != data.confirmNewPassword) throw new CustomError("Nouveaux mots de passe non conformes");
    await userRepository.update({_id: req.currentUser._id, password: data.newPassword});
    res.json({message: 'Mot de passe modifié avec succès'});
}



const login = async function (req, res){
    const user = await UserService.findUserByEmailAndPassword(req.body);
    if(!user) throw new CustomError('Email ou mot de passe invalide')
    const token = await UserService.createToken(user)
    res.json({user, token})
}

const logout = async function (req, res){
    await tokenRepository.destroyToken(req.headers.token);
    res.json({message: "User deconnecte avec succes"});
}

const canAccess = async function (req, res) { 
    let canAccess = req.body.roleIds.includes(req.currentUser.roleId);
    if(req.body.roleIds.length === 0) canAccess = true;
    res.json({canAccess});
}


const getUserData = async function (req, res){
    const user = req.currentUser;
    res.json(user);
}

const getUserInfo = async function (req, res){
    const user = await UserService.findUserForChat(req.params.id);
    res.json(user);
}

const getClients = async function (req, res){
    const result = await UserService.findClients(req.query);
    res.json(result);
}

const getClientById = async function (req, res){
    const result = await UserService.findUserForChat(req.params.id);
    res.json(result);
}


router.post('/signin', createAuth([Constant.roles.admin]) ,createBodySchemaParser(User, 'createSchemaDto'),createRouteCallback(signin));
router.post('/login', createBodySchemaParser(User, 'loginSchemaDto'), createRouteCallback(login));
router.get('/logout',createAuth(), createRouteCallback(logout));
router.post('/can-access',createAuth(), createBodySchemaParser(User, 'canAccessDto'), createRouteCallback(canAccess));
router.get('/user-data',createAuth(), createRouteCallback(getUserData));
router.get('/:id/info',createAuth(), createRouteCallback(getUserInfo));
router.get('/clients', createAuth([Constant.roles.admin]), createRouteCallback(getClients));
router.get('/clients/:id', createAuth([Constant.roles.admin]), createRouteCallback(getClientById));
router.patch('/', createAuth([]), createBodySchemaParser(User, 'updateSchemaDto'), createRouteCallback(updateInfos));
router.patch('/password', createAuth([]), createBodySchemaParser(User, 'updatePasswordSchemaDto'), createRouteCallback(updatePassword));

/*
const createTest  = (n) => function (req, res){
    res.json({message:"Called test "+n})
}
router.get('/test-1', createAuth(), createTest(1))
router.get('/test-2', createAuth([1]), createTest(2))
router.get('/test-3', createAuth([2]), createTest(3))
*/
module.exports = router;