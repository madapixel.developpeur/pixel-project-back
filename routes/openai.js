var express = require('express'); 
const createRouteCallback = require('../commons/functions/create-route-callback');
const getEnv = require('../commons/functions/get-env');
var router = express.Router();


const generateText = async function(req, res) {
    const conversationHistory = [];
    conversationHistory.push({ role: "user", content: req.body.req });
    const completion = await req.app.openai.createChatCompletion({
      model: getEnv('OPENAI_MODEL'),
      messages: conversationHistory,
    });
    const response = completion.data.choices[0].message.content;
    res.json(response);
}

const generateImages = async function(req, res) {
  /* {
        prompt: "A cute baby sea otter",
        n: 2,
        size: "1024x1024"
    } */
    const image = await req.app.openai.createImage(req.body);

    const response = image.data.data;
    res.json(response);
}


router.post('/text', createRouteCallback(generateText));
router.post('/images',  createRouteCallback(generateImages));

module.exports = router;
