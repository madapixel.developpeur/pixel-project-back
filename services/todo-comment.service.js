const { ObjectId } = require("mongodb");
const GenRepository = require("../commons/database/class/gen-repository");
const CustomError = require("../errors/custom-error");
const Constant = require("../models/constant.model");
const TodoComment = require("../models/todo-comment.model");
const TodoService = require("./todo.service");

const todoCommentRepository = new GenRepository(TodoComment);
module.exports = class TodoCommentService {
    

    static async createComment(data, currentUser){
        //const todo = await TodoService.findCoreById(data.todoId, {exists: true, currentUser: (currentUser.roleId == Constant.roles.client ? currentUser : null)});
        data.todoId = new ObjectId(data.todoId);
        data.userId = currentUser._id;
        data.dateComment = new Date();
        await todoCommentRepository.insert([data]);
        return data;
    }

    static async findComments(todoId){
        const todoCommentCollection = GenRepository.getCollectionFrom(TodoComment.collection);
        const sortOptions = GenRepository.createSortOPtions([{column: 'dateComment', order: 'asc'}], true).sort;
        return await todoCommentCollection.aggregate([
            {
                $match: { 
                    todoId: new ObjectId(todoId),
                }
            },
            {
                $lookup: {
                    from: "User",
                    localField: "userId",
                    foreignField: "_id",
                    as: "user"
                }
            },
            {
                $unwind: {
                    path: "$user", 
                }
            },
            { 
                $set: { 
                    "user.name": {
                        $concat: [{$ifNull: ["$user.firstName", ""]}, " ", {$ifNull: ["$user.lastName", ""]}]
                    } 
                } 
            },
            {
                $project: {
                    "user.password": false,
                }
            }
        ]).sort(sortOptions).toArray();
                
    }
}