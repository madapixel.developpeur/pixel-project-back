"use strict";

const env = require('../commons/functions/get-env');
const hbs = require('nodemailer-express-handlebars')
const nodemailer = require('nodemailer')
const path = require('path')

const option = {
    host: env('MAIL_HOST'),
    port: parseInt(env('MAIL_PORT')),
    secure: true, // true for 465, false for other ports
    auth: {
        user: env('MAIL_USER'),
        pass: env('MAIL_PASSWORD')
    },
    tls: {
        rejectUnauthorized: false
    }
};

const handlebarOptions = {
    viewEngine: {
        partialsDir: path.resolve('./emails/'),
        defaultLayout: false,
    },
    viewPath: path.resolve('./emails/'),
};

const transporter = nodemailer.createTransport(option);
transporter.use('compile', hbs(handlebarOptions))

module.exports = class MailerService{
     

    /* let mailOptions = {
        from: "'Mr. Fox 🦊' <user@website.com>", // sender address
        to: "satoshi@bitcoin.com", // list of receivers
        subject: "Hello World", // Subject line
        text: "Hello World?", // plain text body
        html: "<p>Hello World?</p>" // html body
    }; */
    static async sendMail(mail){        
        try {
            mail.from = env('MAIL_FROM');
            if(!mail.context) {
                mail.context = {};
            }
            mail.context.FRONT_URL = env('FRONT_URL');

            let info = await transporter.sendMail(mail);
            return {
                "statut" : 1,
                "message" : "Votre mail est bien envoyé",
                "info" : info
            };
        } catch (error) {
            return {
                "statut" : 0,
                "message" : "Votre mail n'est pas envoyé",
                "error" : error
            };
        }
    }

    static async sendMessageMail(userFrom, userTo){
        let mailOptions = {
            to: userTo.email, // list of receivers
            subject: "Nouveau message sur Pixel Project", // Subject line
            template: 'message',
            context: {userFrom, client: userTo.roleId == 1}
        };
        
        return await this.sendMail(mailOptions);
    }

    static async sendEmailNewClient(client, plainPassword){
        let mailOptions = {
            to: client.email, // list of receivers
            subject: "Bienvenue sur Pixelsior Buzz Boosters", // Subject line
            template: 'new_client',
            context: {client, plainPassword}
        };

        return await this.sendMail(mailOptions);
    }

    static async sendEmailProjectSavedByClient(project, user){
        let mailOptions = {
            to: user.email, // list of receivers
            subject: "Projet en attente de validation", // Subject line
            template: 'project_saved_by_client',
            context: {project, user}
        };

        return await this.sendMail(mailOptions);
    }

    static sendEmailProjectSavedByClientToAllAdmins(project, admins){
        admins.forEach((user) => {
            this.sendEmailProjectSavedByClient(project, user);
        });
    }

    static async sendEmailProjectValidatedByAdmin(project, client){
        let mailOptions = {
            to: client.email, // list of receivers
            subject: "Projet validé", // Subject line
            template: 'project_validated_by_admin',
            context: {project, client}
        };

        return await this.sendMail(mailOptions);
    }

    static async sendEmailTodosValidatedByClient(project, user){
        let mailOptions = {
            to: user.email, // list of receivers
            subject: "Tâches validées par le client", // Subject line
            template: 'todo_validated_by_client',
            context: {project, user}
        };

        return await this.sendMail(mailOptions);
    }

    static sendEmailTodosValidatedByClientToAllAdmins(project, admins){
        admins.forEach((user) => {
            this.sendEmailTodosValidatedByClient(project, user);
        });
    }

    static async sendEmailTodoFinished(todo, client){
        let mailOptions = {
            to: client.email, // list of receivers
            subject: "Tâche finie", // Subject line
            template: 'task_finished',
            context: {todo, client}
        };
        console.log(mailOptions);

        return await this.sendMail(mailOptions);
    }

    static async sendEmailAllTaskValidated(project, client){
        let mailOptions = {
            to: client.email, // list of receivers
            subject: "Projet achevé", // Subject line
            template: 'all_task_validated',
            context: {project, client}
        };
        console.log(mailOptions);

        return await this.sendMail(mailOptions);
    }
}