const { ObjectId } = require("mongodb");
const GenRepository = require("../commons/database/class/gen-repository");
const CustomError = require("../errors/custom-error");
const Constant = require("../models/constant.model");
const Project = require("../models/project.model");
const User = require("../models/user.model");
const MailerService = require("./mailer.service");
const UserService = require("./user.service");

const projectRepository = new GenRepository(Project);
module.exports = class ProjectService {
    

    static async insertUpdate(newProject, isInsert=true, currentUser, status=null){
        
        if(status !== null && status !== undefined){
            newProject.status = status;
        }

        if(isInsert){
            //newProject.userId = currentUser._id;
            newProject.creationDate = new Date();
            newProject.details = [];
            newProject.userId = new ObjectId(newProject.userId);
            newProject.status = Constant.projectStatus.created;
            await projectRepository.insert([newProject]);
            return newProject;
        } 
        let project = await projectRepository.findById(newProject._id);
        let projectSavedByClient = false;
        if(currentUser.roleId == Constant.roles.client){
            if(!project.userId.equals(currentUser._id)) throw new CustomError("Pas d'authorisation");
            if(project.status == Constant.projectStatus.created){
                newProject.status = Constant.projectStatus.updated;
                projectSavedByClient = true;
                
            } 
        }
    
        await projectRepository.update(newProject);
        project = await projectRepository.findById(project._id);
        if(projectSavedByClient){
            const admins = await UserService.findAdmins();
            MailerService.sendEmailProjectSavedByClientToAllAdmins(project, admins);
        }
        return project;
    }

    static async insertWithoutAuth(newProject){
        newProject.creationDate = new Date();
        newProject.details = [];
        newProject.userId = new ObjectId(newProject.userId);
        newProject.status = Constant.projectStatus.created;
        await projectRepository.insert([newProject]);
        return newProject;
    }

    static async findCoreProjectById(_id, options = {}){
        const filter = [{
            column: '_id',
            type: 'string',
            value: ObjectId(_id),
            comparator: '='
        }];
        const result = await projectRepository.find({filter});
        if(result.data.length === 0) {
            if(options.exists) throw new CustomError('Aucun projet correspondant')    
            else return null;
        }
        if(options.currentUser && !options.currentUser._id.equals(result.data[0].userId )) 
            throw new CustomError(`Le projet ${result.data[0].name} n'appartient pas a l'utilisateur actuel`);
        if(!options.alsoDeleted && result.data[0].status === Constant.projectStatus.deleted)
            throw new CustomError('Le projet est déjà supprimé');
        if(!options.alsoCreating && result.data[0].status === Constant.projectStatus.creating)
            throw new CustomError('Le projet est en cours de création');
        return result.data[0];
    }

    
    
    static async findProjects(params, options = {}){
        
        if(!params.filter) params.filter = [];
        params.filter.push({
            column: 'status',
            type: 'int',
            value: Constant.projectStatus.created,
            comparator: '>='
        })
        if(options.currentUser){
            params.filter.push({
                column: 'userId',
                type: 'string',
                value: options.currentUser._id,
                comparator: '='
            })
        }
        if(params.pagination && !params.pagination.orderBy){
            params.pagination.orderBy = [ { column: 'creationDate', order: 'desc' } ];
        }

        const projectCollection = GenRepository.getCollectionFrom(Project.collection);
        
        const pagination = GenRepository.createPaginationOptions(params.pagination, true);
        let $match = GenRepository.createMatchOptions(params.filter, params.filterMode);
        if(params.aggregate) $match = {
            ...$match, 
            ...params.aggregate
        };
        let aggregation = [
            {
                $lookup: {
                    from: User.collection,
                    localField: 'userId',
                    foreignField: '_id',
                    as: "client"
                }
                
            },
            {
                $unwind: {
                    path: "$client", 
                    preserveNullAndEmptyArrays: false
                }
            },
            { 
                $set: { 
                    clientName: {
                        $concat: [{$ifNull: ["$client.firstName", ""]}, " ", {$ifNull: ["$client.lastName", ""]}]
                    } 
                } 
            },
            { $match }
            
        ];
        
        console.log(aggregation);
        let results = projectCollection.aggregate(aggregation);
        
        const totalResults = await projectCollection.aggregate([...aggregation, {$count: "total"}]).toArray();
        
        if(pagination.limit){
            results = results
            .sort(pagination.sort)
            .skip(pagination.skip)
            .limit(pagination.limit);            
        }

        
        return {
            data: await results.toArray(),
            meta: {
                totalElmtCount: totalResults.length > 0 ? totalResults[0]['total'] : 0
            }
        }
        
    }

    static async findProjectsWithRoles(params, user,options = {}){
        if(user.roles != null && Array.isArray(user.roles)) {

            // {
            //     $or : [ 
            //         {"type._id": ObjectId('6413043f399c52ef0bc02be6')},
            //         {"type._id": ObjectId('6413034f399c52ef0bc02bdf')}
            //     ],
            //     "status": 3  
            // }

            params.aggregate = {
                $or : []
            };
            user.roles.forEach(role => {
                params.aggregate.$or.push({"type._id": new ObjectId(role)});
            });
            
        }
        return this.findProjects(params, options);
    }

    static async changeStatus(_id, status, options = {}){
        options.exists = true;
        const project = await this.findCoreProjectById(_id, options);
        const projectId = project._id;
        project.status = status;
        await projectRepository.update(project);
        project._id = projectId;
        if(status == Constant.projectStatus.validatedByAdmin){
            const client = await UserService.findUserById(project.userId);
            MailerService.sendEmailProjectValidatedByAdmin(project, client);
        } else if (status == Constant.projectStatus.todosValidatedByClient) {
            const admins = await UserService.findAdmins();
            MailerService.sendEmailTodosValidatedByClientToAllAdmins(project, admins);
        }
    }

    static async bill(data, options = {}){
        const project = await this.findCoreProjectById(data._id, options);
        data.status = Constant.projectStatus.billed;
        await projectRepository.update(data);
    }

    static async updateDateLivraison(data){
        await projectRepository.update({_id: data._id, dateLivraison: data.dateLivraison})
    }
}