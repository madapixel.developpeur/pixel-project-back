const { ObjectId } = require("mongodb");
const GenRepository = require("../commons/database/class/gen-repository");
const CustomError = require("../errors/custom-error");
const Constant = require("../models/constant.model");
const Todo = require("../models/todo.model");
const MailerService = require("./mailer.service");
const ProjectService = require("./project.service");
const UserService = require("./user.service");

const todoRepository = new GenRepository(Todo);
module.exports = class TodoService {
    

    static async createTodo(data){
        const project = await ProjectService.findCoreProjectById(data.projectId, {exists: true});
        data.creationDate = new Date();
        data.projectId = ObjectId(data.projectId);
        data.status = Constant.todoStatus.created;
        data.progression = 0;
        await todoRepository.insert([data]);
        return data;
    }

    static async deleteTodo(todoId){
        const todo = await this.findCoreById(todoId, {exists: true});
        todo.status = Constant.todoStatus.deleted;
        await todoRepository.update(todo);
    }


    static async updateTodo(data, options = {}){
        const id = data._id;
        const todo = await this.findCoreById(id, {exists: true, currentUser: options.currentUser});
        data.status = data.status??todo.status; 
        if (data.status < Constant.todoStatus.inProgress) data.progression = 0;
        else if (data.status > Constant.todoStatus.inProgress) data.progression = 100;
        await todoRepository.update(data);
        return await this.findCoreById(id, {addProject: options.addProject});
    }

    static async findCoreById(_id, options = {}){
        const filter = [{
            column: '_id',
            type: 'string',
            value: new ObjectId(_id),
            comparator: '='
        }];
        const result = await todoRepository.find({filter});
        if(result.data.length === 0){ 
            if(options.exists) throw new CustomError('Aucun element correspondant')    
            else return null;
        }
        if(options.currentUser || options.addProject){
            const project = await ProjectService.findCoreProjectById(result.data[0].projectId, {exists: true, currentUser: options.currentUser});
            result.data[0].project = project;
        }
        if(!options.alsoDeleted && result.data[0].status === Constant.todoStatus.deleted)
            throw new CustomError("L'element est déjà supprimé");
        return result.data[0];
    }

    static async findTodosByProject(projectId, params, options = {}){
        if(options.currentUser){
            const project = await ProjectService.findCoreProjectById(projectId, {exists: true, currentUser: options.currentUser});
        }
        if(!params.filter) params.filter = [];
        params.filter.push({
            column: 'projectId',
            type: 'string',
            value: ObjectId(projectId),
            comparator: '='
        })
        params.filter.push({
            column: 'status',
            type: 'int',
            value: Constant.todoStatus.deleted,
            comparator: '!='
        })
        if(params.pagination && !params.pagination.orderBy){
            params.pagination.orderBy = [ { column: 'creationDate', order: 'desc' } ];
        }
        return await todoRepository.find(params)
    }


    static async findTodosByStatus(projectId, status, params, options = {}){
        params = JSON.parse(JSON.stringify(params));
        if(!params.filter) params.filter = [];
        params.filter.push({
            column: 'status',
            type: 'int',
            value: status,
            comparator: '='
        })
        
        return await this.findTodosByProject(projectId, params, options);
    }

    static async findTodosClassifiedByStatus(projectId, params, options = {}){
        return {
            todo: (await this.findTodosByStatus(projectId, Constant.todoStatus.created, params, options)).data,
            inProgress: (await this.findTodosByStatus(projectId, Constant.todoStatus.inProgress, params, options)).data,
            finished: (await this.findTodosByStatus(projectId, Constant.todoStatus.finished, params, options)).data,
            validated: (await this.findTodosByStatus(projectId, Constant.todoStatus.validated, params, options)).data,
        };
    }

    static async updateProgression(data, options = {}){
        let dateKey = null;
        switch(data.status){
            case Constant.todoStatus.created:
                data.progression = 0;
                break;
            case Constant.todoStatus.inProgress:
                dateKey = "inProgressDate";
                break;
            case  Constant.todoStatus.finished:
                data.progression = 100;
                dateKey = "finishedDate";
                break;
            case  Constant.todoStatus.validated:
                data.progression = 100;
                dateKey = "validatedDate";
                break;
            default:
                break;
        }
        if(dateKey) data[dateKey] = data.progressionDate;
        delete data.progressionDate;
        const result = await this.updateTodo(data, {...options, addProject: data.status == Constant.todoStatus.finished});
        if(data.status == Constant.todoStatus.finished){
            const client = await UserService.findUserById(result.project.userId);
            MailerService.sendEmailTodoFinished(result, client);
        } else if(data.status == Constant.todoStatus.validated){
            
            const project = await this.findProjectProgression(result.projectId);
            if(project.todosAllValidated){
                const client = await UserService.findUserById(project.userId);
                MailerService.sendEmailAllTaskValidated(project, client);
            }
        }
        return result;
    }
    
    static async updateProgressionValue(data){
        const todo = await this.findCoreById(data._id, {exists: true});
        if(todo.status !== Constant.todoStatus.inProgress) 
            throw new CustomError("La tâche n'est pas en cours");
        await todoRepository.update(data);
    }

    static async findProjectProgression(_id, options = {}, params = {}){
        const project = await ProjectService.findCoreProjectById(_id, options);
        delete options.currentUser;
        project.todos = await this.findTodosClassifiedByStatus(_id, params, options);
        project.todosLength = project.todos.todo.length + project.todos.inProgress.length + project.todos.finished.length + project.todos.validated.length;
        
        let progression = 0;
        let todosAllValidated = false;
        if(project.todosLength > 0){
            progression = (project.todos.finished.length + project.todos.validated.length) * 100;
            project.todos.inProgress.forEach(element => {
                progression += element.progression;
            });
            progression /= project.todosLength;
            if(project.todos.validated.length === project.todosLength){
                todosAllValidated = true;
            }
        }

        project.progression = progression;
        project.todosAllValidated = todosAllValidated;

        return project;
    }
}