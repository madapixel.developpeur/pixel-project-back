const { ObjectId } = require("mongodb");
const GenRepository = require("../commons/database/class/gen-repository");
const { getClient, getConnection, transactionOptions } = require("../configs/db");
const CustomError = require("../errors/custom-error");
const Constant = require("../models/constant.model");
const MessageSeen = require("../models/message-seen.model");
const Message = require("../models/message.model");
const User = require("../models/user.model");
const MailerService = require("./mailer.service");
const UserService = require("./user.service");

const messageRepository = new GenRepository(Message);
module.exports = class MessageService {

    static getRolesChat(currentUser){
        let roles;
        if(currentUser.roleId == Constant.roles.client) roles = [Constant.roles.admin]; 
        else if(currentUser.roleId == Constant.roles.admin) roles = [Constant.roles.client];
        return roles;
    }

    static async insertUpdateMs(ms, options = {}){
        const messageSeenCollection = GenRepository.getCollectionFrom(MessageSeen.collection);
        const results = await messageSeenCollection.find({userFromId: ms.userFromId, userToId: ms.userToId}).toArray();
        let result = results.length > 0 ? results[0] : null;
        if(result){
            await messageSeenCollection.updateOne({_id: result._id}, {$set: ms}, options);
            return Object.assign(result, ms);
        }  else{
            ms._id = new ObjectId();
            await messageSeenCollection.insertOne(ms, options);
            return ms;
        }
    }

    static async send(data, currentUser, io){
        const session = getClient().startSession();
        const options = {session};
        try {
            session.startTransaction(transactionOptions);
            const messageCollection = GenRepository.getCollectionFrom(Message.collection);
            const userTo = await UserService.findUserForChat(data.userToId);

            data.userToId = userTo._id;
            data.userFromId = currentUser._id;
            data.sentDate = new Date();
            data.status = Constant.messageStatus.sent;
            await messageCollection.insertOne(data, options);

            const ms1 = {
                userFromId: data.userFromId, 
                userToId: data.userToId, 
                lastMessageId: data._id,
                lastSeenDate: new Date()
            };
            await this.insertUpdateMs(ms1, options);

            const ms2 = {
                userFromId: data.userToId, 
                userToId: data.userFromId, 
                lastMessageId: data._id,
            };
            await this.insertUpdateMs(ms2, options);
            await session.commitTransaction();  
            MailerService.sendMessageMail(currentUser, userTo)
            .then((res) => {
                console.log(res)
            })
            .catch((err) => {
                console.error(err);
            });
        } catch(err){
            await session.abortTransaction();
            throw err;
        } finally {
            await session.endSession();
        }

        io.emit('message', {userToId: data.userToId, userFromId: data.userFromId, messageId: data._id});
        return data;
    }

    static async findChatUsers(currentUser, params = {}){
        const userCollection = GenRepository.getCollectionFrom(User.collection);
        
        if(!params.orderBy){
            params.orderBy = [{column: 'message.sentDate', order: 'desc'}, {column: '_id', order: 'asc'}]
        }
        
        const options = {
            ...GenRepository.myCreatePaginationOptions(params.pagination),
            ...GenRepository.createSortOPtions(params.orderBy, true)
        };

        console.log(params);
        console.log(options);

        let results = userCollection.aggregate([
            {
                $match: { 
                    _id: {$ne: currentUser._id},
                    roleId: {$in: this.getRolesChat(currentUser)}
                }
            },
            { 
                $set: { 
                    name: {
                        $concat: [{$ifNull: ["$firstName", ""]}, " ", {$ifNull: ["$lastName", ""]}]
                    } 
                } 
            },
            {
                $match: { 
                    name: {'$regex': params.search ? params.search.trim() : '', '$options': 'i' },
                }
            },
            {
                $lookup: {
                    from: "MessageSeen",
                    let: { u_id: "$_id" },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ["$userFromId", currentUser._id] },
                                        { $eq: ["$userToId", "$$u_id"]}
                                    ]
                                }
                            }
                        }
                    ],
                    as: "ms"
                }
            },
            {
                $unwind: {
                    path: "$ms", 
                    preserveNullAndEmptyArrays: true
                }
            },
            /*{ 
                $set: { 
                    lastMessageId: {
                        $cond: [
                            {
                                $or: [
                                    {ms: { $exists: false} }, 
                                    {ms: null}
                                ]
                            },
                            null,
                            "$ms.lastMessageId"
                        ]
                    },
                    lastSeenDate: {
                        $cond: [
                            {
                                $or: [
                                    {ms: { $exists: false} }, 
                                    {ms: null}
                                ]
                            },
                            null,
                            "$ms.lastSeenDate"
                        ]
                    }
                    
                } 
            },*/
            {
                $lookup: {
                    from: "Message",
                    localField: "ms.lastMessageId",
                    foreignField: "_id",
                    as: "message"
                }
            },
            {
                $unwind: {
                    path: "$message", 
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $project: {
                    ms: true,
                    name: true,
                    _id: true,
                    message: true,
                    email: true,
                    image: true
                }
            }
        ]);
        
        if(options.sort){
            results = results
            .sort(options.sort);
        }

        if(options.limit){
            results = results
            .skip(options.skip)
            .limit(options.limit)
        }
        
        return {
            data: await results.toArray(),
            meta: {
                totalElmtCount: 0
            }
        }
        
    }

    static async findMessages(currentUser, userId, params = {}){
        const messageCollection = GenRepository.getCollectionFrom(Message.collection);
        params.orderBy = [{column: 'sentDate', order: 'desc'}, {column: '_id', order: 'desc'}]
        const options = {
            ...GenRepository.myCreatePaginationOptions(params.pagination),
            ...GenRepository.createSortOPtions(params.orderBy)
        };

        const filters = {
            $or: [
                {userFromId: currentUser._id, userToId: userId}, 
                {userFromId: userId, userToId: currentUser._id},
            ],
        }
        let search = params.search ?  params.search.trim() : '';
        if(search != ''){
            filters.contenu = {'$regex': params.search ? params.search.trim() : '', '$options': 'i' };
        } 

        console.log(filters);
        const results = await messageCollection.find(filters,options).toArray();
        
        return {
            data: results,
            meta: {
                totalElmtCount: await messageCollection.countDocuments(filters)
            }
        }
    }

    static async findMessageById(messageId, currentUser){
        const filters = {
            $or: [
                {userFromId: currentUser._id}, 
                {userToId: currentUser._id},
            ],
            _id: messageId
        };
        const messageCollection = GenRepository.getCollectionFrom(Message.collection);
        const results = await messageCollection.find(filters).toArray();
        return results.length > 0 ? results[0] : null; 
    }

    static async setSeenDateNow(currentUser, userToId, io){
        const ms = {
            userFromId: currentUser._id, 
            userToId, 
            lastSeenDate: new Date()
        };
        const res = await this.insertUpdateMs(ms);
        io.emit('seen', {userToId: res.userToId, userFromId: res.userFromId, seenId: res._id});
        return res;
    }

    static async findSeenById(seenId, currentUser){
        const filters = {
            $or: [
                {userFromId: currentUser._id}, 
                {userToId: currentUser._id},
            ],
            _id: seenId
        };
        const messageSeenCollection = GenRepository.getCollectionFrom(MessageSeen.collection);
        const results = await messageSeenCollection.find(filters).toArray();
        return results.length > 0 ? results[0] : null; 
    }

    static async findSeenUser(userId, currentUser){
        const filters = {
            userFromId: userId, 
            userToId: currentUser._id,
        };
        const messageSeenCollection = GenRepository.getCollectionFrom(MessageSeen.collection);
        const results = await messageSeenCollection.find(filters).toArray();
        return results.length > 0 ? results[0] : null; 
    }


    static async findMessagesNotSeen(currentUser){
        const userCollection = GenRepository.getCollectionFrom(User.collection);
        let results = await userCollection.aggregate([
            {
                $match: { 
                    _id: {$ne: currentUser._id},
                    roleId: {$in: this.getRolesChat(currentUser)}
                }
            },
            { 
                $set: { 
                    name: {
                        $concat: [{$ifNull: ["$firstName", ""]}, " ", {$ifNull: ["$lastName", ""]}]
                    } 
                } 
            },
            {
                $lookup: {
                    from: "MessageSeen",
                    let: { u_id: "$_id" },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ["$userFromId", currentUser._id] },
                                        { $eq: ["$userToId", "$$u_id"]}
                                    ]
                                }
                            }
                        }
                    ],
                    as: "ms"
                }
            },
            {
                $unwind: {
                    path: "$ms", 
                }
            },
            { 
                $set: { 
                    "ms.lastSeenDate": {$ifNull: ["$ms.lastSeenDate", null]}
                } 
            },
            {
                $lookup: {
                    from: "Message",
                    let: { lastMessageId: "$ms.lastMessageId", lastSeenDate: "$ms.lastSeenDate" },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ["$userToId", currentUser._id] },
                                        { $eq: ["$_id", "$$lastMessageId"] },
                                        { $or: [
                                            {$gt: ["$sentDate", "$$lastSeenDate"]},
                                            {$eq: [null, "$$lastSeenDate"]},
                                        ]}
                                    ]
                                }
                            }
                        }
                    ],
                    as: "message"
                }
            },
            {
                $unwind: {
                    path: "$message", 
                }
            },
            {
                $project: {
                    ms: true,
                    name: true,
                    _id: true,
                    message: true,
                    email: true,
                    image: true
                }
            }
        ]).toArray();
        
        
        
        return {
            data: results,
            meta: results.length
        }
        
    }

}