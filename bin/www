#!/usr/bin/env node

/**
 * Module dependencies.
 */

var app = require('../app');
var debug = require('debug')('garage-server:server');
var http = require('http');
const { Configuration, OpenAIApi } = require("openai");
const getEnv = require('../commons/functions/get-env');




/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

/**
 * Create HTTP server.
 */
const configuration = new Configuration({
  apiKey: getEnv('OPENAI_API_KEY'),
});

const openai = new OpenAIApi(configuration);

var server = http.createServer(app);
const io = require('socket.io')(server, {
  cors: {origin : '*'}
});
io.on('connection', (socket) => {
  console.log('a user connected');

  socket.on('message', (message) => {
    console.log(message);
  });

  socket.on('disconnect', () => {
    console.log('a user disconnected!');
  });

  //openAI
  const conversationHistory = [];

  const TYPES = {
    OPENAI_MESSAGE_CHATBOT: 1,
    OPENAI_MESSAGE_GENERATE_TEXT: 2,
    OPENAI_MESSAGE_GENERATE_IMAGES: 3
  }

  socket.on("messageOpenAI", async (req) => {
    try {
      let response = null;
      if(req.type == TYPES.OPENAI_MESSAGE_CHATBOT || req.type == TYPES.OPENAI_MESSAGE_GENERATE_TEXT){
        conversationHistory.push({ role: "user", content: req.req });

        const completion = await openai.createChatCompletion({
          model: getEnv('OPENAI_MODEL'),
          messages: conversationHistory,
        });

        response = completion.data.choices[0].message.content;

        conversationHistory.push({ role: "assistant", content: response });
        
      } else if(req.type == TYPES.OPENAI_MESSAGE_GENERATE_IMAGES){
        /* {
            prompt: "A cute baby sea otter",
            n: 2,
            size: "1024x1024"
        } */
        const image = await openai.createImage(req.req);

        response = image.data.data;
       
      }
      socket.emit("responseOpenAI", {type: req.type, content: response});
    } catch (error) {
      console.error(error);
      socket.emit("responseOpenAI", {type: req.type, message: 'Une erreur est survenue merci de réessayer', error: true});
    }
  });
});

app.io = io;
app.openai = openai;

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
  console.log('Listening on ' + bind);
}
