
const uploadRoutes = require('./routes/upload')
const usersRoutes = require('./routes/users')
const projectsRoutes = require('./routes/projects')
const todosRoutes = require('./routes/todos')
const typesProject = require('./routes/types-project')
const projectClientRoute = require('./routes/project-client')
const downloadRoutes = require('./routes/download')
const messagesRoutes = require('./routes/messages')
const todoComments = require('./routes/todo-comments')
const openaiRoutes = require('./routes/openai')

module.exports = function (app){
    app.use("/upload", uploadRoutes);
    app.use("/download", downloadRoutes);
    app.use("/users", usersRoutes);
    app.use("/project-client", projectClientRoute);
    app.use("/projects", projectsRoutes);
    app.use("/todos", todosRoutes);
    app.use("/types-project", typesProject);
    app.use("/messages", messagesRoutes);
    app.use("/todo-comments", todoComments);
    app.use("/openai", openaiRoutes);
}