const multer = require('multer');
const PATH = './uploads';
function getFileExtension(filename){
    // get file extension
    const extension = filename.substring(filename.lastIndexOf('.') + 1, filename.length);
    return extension;
} 

let storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, PATH);
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now() + '.' + getFileExtension(file.originalname))
    }
});

let upload = multer({
    storage: storage
});

module.exports = upload;


